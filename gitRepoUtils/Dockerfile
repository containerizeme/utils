ARG BASE_IMG_VERSION=3.21.0
FROM alpine:${BASE_IMG_VERSION}

# Dummy ARGs for user onbuild workaround
## User is created during the 'FROM' command in the deriving image
ONBUILD ARG USER
ONBUILD ARG GROUP
ONBUILD ARG UID
ONBUILD ARG GID
ONBUILD ARG REPO_DIR

# Repo settings and paths
ONBUILD ENV REPO_DIR=${REPO_DIR}
ENV REPO_URL=
ENV GIT_HELPERS_DIR=/opt/gitHelperTools
ENV GIT_CONFIG_DIR=/srv/gitConfig

RUN apk add --no-cache \
  git=2.47.1-r0 \
  git-lfs=3.6.0-r0 \
  openssh-client-default=9.9_p1-r2 \
  openssh-keygen=9.9_p1-r2

# Copy the git helper tools and make them executable
COPY /utils/ ${GIT_HELPERS_DIR}/
RUN chmod -R 755 ${GIT_HELPERS_DIR}/*.sh

# Create user in derived image
ONBUILD RUN addgroup -g ${GID} ${GROUP} && \
  adduser -u ${UID} -G ${GROUP} -D ${USER}

# Prepare git configuration and ssh directory
# Prepare known hosts
RUN mkdir -p ${GIT_CONFIG_DIR}/.ssh/ && \
  touch ${GIT_CONFIG_DIR}/.gitconfig && \
  touch ${GIT_CONFIG_DIR}/.ssh/known_hosts && \
  cat ${GIT_HELPERS_DIR}/resources/known_hostsGithub >> ${GIT_CONFIG_DIR}/.ssh/known_hosts && \
  cat ${GIT_HELPERS_DIR}/resources/known_hostsGitLab >> ${GIT_CONFIG_DIR}/.ssh/known_hosts

# Sym links to git configuration for the container user and access
ONBUILD RUN ln -s ${GIT_CONFIG_DIR}/.ssh/ /home/${USER}/.ssh && \
  ln -s ${GIT_CONFIG_DIR}/.gitconfig /home/${USER}/.gitconfig && \
  chown -R ${USER}:${GROUP} ${GIT_CONFIG_DIR}

# Prepare repo directory
ONBUILD RUN mkdir -p ${REPO_DIR} && \
  chown -R ${USER}:${GROUP} ${REPO_DIR}

ONBUILD VOLUME ${GIT_CONFIG_DIR}

CMD ["git"]