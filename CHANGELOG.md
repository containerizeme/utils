- [Git Repo Utils](#git-repo-utils)

# Git Repo Utils

### 3.21.0-r1-onbuild
* Update Alpine 3.21.0
* git 2.47.1-r0
* git-lfs 3.6.0-r0
* openssh-client-default 9.9_p1-r2
* openssh-keygen 9.9_p1-r2

### 3.20.3-r1-onbuild
* Update Alpine 3.20.3
* git 2.45.2-r0
* git-lfs 3.5.1-r4
* openssh-client-default 9.7_p1-r4
* openssh-keygen 9.7_p1-r4


### 3.19.1-r3-onbuild
* Update git-lfs 3.4.1-r2

### 3.19.1-r2-onbuild
* Update git-lfs 3.4.1-r1

### 3.19.1-r1-onbuild
* Alpine 3.19.1
* Added git-lfs 3.4.1-r0

### 3.19.0-r1-onbuild
* Alpine 3.19.0
* git 2.43.0-r0
* openssh 9.6_p1-r0

### 3.18.4-r1-onbuild
* Alpine 3.18.4
* git 2.40.1-r0
* openssh 9.3_p2-r0

### 3.18.0-r1-onbuild
* Alpine 3.18.0
* git 2.40.1-r0
* Removed entrypoint

### 3.17.3-r2-onbuild
* git 2.38.5-r0

### 3.17.3-r1-onbuild
* Alpine 3.17.3
* git 2.38.4-r1

### 3.17.2-r1-onbuild
* Alpine 3.17.2
* git 2.38.4-r0
* openssh9.1_p1-r2

### 3.17.0-r1-onbuild
* Alpine 3.17.0
* git 2.38.1-r0
* openssh 9.1_p1-r1

### 3.16.2-r1-onbuild
* Alpine 3.16.2

### 3.16.1-r1-onbuild
* Update to alpine 3.16.1
* git 2.36.2-r0
* openssh 9.0_p1-r2

### 3.15.0-r2-onbuild
* Rebuild for alpine package dependency update
* git 2.34.1-r0
* openssh 8.8_p1-r1

### 3.15.0-r1-onbuild
* git 2.34.1-r0
* openssh 8.8_p1-r1
* Update to Alpine 3.15.0

### 3.14.3-r1-onbuild
* Update to Alpine 3.14.3

### 3.14.2-r0-onbuild
* Update to Alpine 3.14.2

### 3.14.0-r1-onbuild
* Update to Alpine 3.14.0
* git git-2.32.0-r0
* openssh 8.6_p1-r2

### 3.13.5-r0-onbuild
* Update to Alpine 3.13.5

### 3.13.3-r0-onbuild
* Update to Alpine 3.13.3
* git 2.30.2-r0
* opensssh 8.4_p1-r3

### 3.12.0-r0-onbuild
* Update to Alpine 3.12.0
* git 2.26.2-r0
* openssh 8.3_p1-r0

### 3.11.3-r2-onbuild
* Update git to 2.24.3-r0

### 3.11.3-r1-onbuild
* Alpine 3.11.3

### 3.11.2-r1-onbuild
* Alpine 3.11.2, add GitLab as known host

### 3.10.3-r1-onbuild
* Alpine 3.10.3

### 3.10.1-r2-onbuild
* Alpine 3.10.1, on build hooks for user creation

### 3.10.1-r1
* Alpine 3.10.1

### 3.10.0-r1
* Alpine 3.10.0

### 3.9.4-r1
* Alpine 3.9.4, fix version of installed packages

### 3.9-r1
* Alpine 3.9

### 3.8-r2]
* Alpine 3.8, renamed env variables and changed paths

### 3.8-r1
* Alpine 3.8, initial version
