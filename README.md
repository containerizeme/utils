[![Open Issues](https://img.shields.io/badge/dynamic/json?color=yellow&logo=gitlab&label=open%20issues&query=%24.statistics.counts.opened&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17716392%2Fissues_statistics)](https://gitlab.com/containerizeme/utils/-/issues)
[![Last Commit](https://img.shields.io/badge/dynamic/json?color=green&logo=gitlab&label=last%20commit&query=%24[:1].committed_date&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17716392%2Frepository%2Fcommits%3Fbranch%3Dmaster)](https://gitlab.com/containerizeme/utils/-/commits/master)

[![License](https://img.shields.io/badge/dynamic/json?color=orange&label=license&query=%24.license.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F17716392%3Flicense%3Dtrue)](https://gitlab.com/containerizeme/utils/-/blob/master/LICENSE)
applies to software of this project. The running software within the image/container ships with its own license(s).

[![GitRepoUtils](https://badgen.net/badge/project/GitRepoUtils/orange?icon=gitlab)](https://gitlab.com/containerizeme/utils/-/blob/master/README.md#git-repo-utils)
[![Stable Version](https://img.shields.io/docker/v/icebear8/gitrepoutils/stable?color=informational&label=stable&logo=docker)](https://gitlab.com/containerizeme/utils/-/blob/master/CHANGELOG.md#git-repo-utils)
[![Docker Pulls](https://badgen.net/docker/pulls/icebear8/gitrepoutils?icon=docker&label=pulls)](https://hub.docker.com/r/icebear8/gitrepoutils)
[![Docker Image Size](https://badgen.net/docker/size/icebear8/gitrepoutils/stable?icon=docker&label=size)](https://hub.docker.com/r/icebear8/gitrepoutils)

#   Git Repo Utils

This image contains basic scripts to access a git repository.
This image may be used as base layer to build other services on top of it.
The image is prepared to access a remote git repository.
On build hooks are used to setup the base image for a specific user

##  Usage
`docker run -e REPO_URL=git@github.com:<someRepository> --name nginx icebear8/gitrepoutils:latest`

## ONBUILD Hooks
The arguments for the ONBUILD arguments must be given before the `FROM` statement.
```
ARG USER=me
ARG GROUP=myself
ARG UID=6666
ARG GID=6666
ARG REPO_DIR=/opt/repo
FROM icebear8/gitrepoutils:latest
```

| ONBUILD ARG | Description                 |
|-            |-                            |
| USER        | User to be created and used |
| GROUP       | User group to be used       |
| UID         | The user id to be used      |
| GID         | The group id to be used     |
| REPO_DIR    | Working directory with the repository |

##  Environment Variables

| Variable        | Description |
|-                |-            |
| REPO_URL        | The URL of the remote git repository |
| REPO_DIR        | The local target location of the repository (clone) |
| GIT_HELPERS_DIR | Location where the git repo utils scripts are located |
| GIT_CONFIG_DIR  | Location where the git configuration and .ssh data is stored |

##  Git Repo Utils Scripts

| Script              | Description |
|-                    |-            |
| initGitUser.sh      | Creates the current user as the global git user with a fake mail address  |
| printPublicKey.sh   | Prints the current public key (if existing)
| reinitKey.sh        | Reinitializes the private/public key pair |
| repoClone.sh        | Clones the repository from `REPO_URL` into `REPO_DIR` |
| repoUpdate.sh       | Updates the repository from `REPO_URL`
| setupRepoAccess.sh  | Initializes the git user and the keys if no keys exist; tries to initially clone or update the repo on each run |

##  Usage in Docker Containers
Use the environment variable `REPO_URL` to link the container to a specific git repository.

* At container startup, call `setupRepoAccess.sh` to
  * Create a new git user and private/public key pair if they do not exist
  * Try to clone the repository at each container start
  * This will fail as long as the public key is not added to the remote repository
* The variable `GIT_CONFIG_DIR` points to the location where the gitconfig and .ssh data is stored
  * A symlink from `~/.ssh` and `~/.gitconfig`
* The public key is printed on the shell at the first startup
  * Use `docker logs <container>` to print the log after the startup
  * Or use the command `docker exec <container> ${GIT_HELPERS_DIR}/printPublicKey.sh` to print the public key
* Add the public key to your Github repository with the website content
  * Settings/Deploy keys/Add deploy key
  * Read access is enough for most of the containers
* Clone the website content
  * Stop and start the container again
  * Or execute `docker exec <container> ${GIT_HELPERS_DIR}/repoClone.sh`

##  Resources
* known_hostsGithub: Public key identification of the Github host
* known_hotsGitLab: Public key identification of GitLab host
